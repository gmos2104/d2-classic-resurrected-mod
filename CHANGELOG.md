# D2 Resurrected changes

- Ladder-only unique items available in single player mode.
- Ladder-only rune words available in single player mode.
- Ladder-only cube recipes available in single player mode.

# D2 Resurrected: Patch 2.4

## Class Changes

### Assassin

See #2 and #3 for remaining issues.

#### Martial Arts

- Fist of Fire

  - Attack Rating bonus increased from +15% to +25% baseline, and from +7% to +10% per level

- Claws of Thunder

  - Attack Rating bonus increased from +15% to +25% baseline, and from +7% to +10% per level

- Blades of Ice

  - Attack Rating bonus increased from +15% to +25% baseline, and from +7% to +10% per level

- Tiger Strike

  - Attack Rating bonus increased from +15% to +25% baseline, and from +7% to +10% per level

- Cobra Strike

  - Attack Rating bonus increased from +15% to +25% baseline, and from +7% to +10% per level

- Phoenix Strike

  - Attack Rating bonus increased from +15% to +25% baseline, and from +7% to +10% per level

- Dragon Talon

  - Now consumes only 1 of each Martial Arts charge when cast
  - Now always hits the target only if the Assassin has any Martial Arts charges

- Dragon Claw

  - Now consumes only 1 of each Martial Arts charge when cast
  - Now always hits the target only if the Assassin has any Martial Arts charges
  - Damage bonus per level increased from 5% to 15%

- Dragon Tail

  - Now consumes only 1 of each Martial Arts charge when cast
  - Now always hits the target only if the Assassin has any Martial Arts charges
  - Damage bonus per level increased from 10% to 20%

- Dragon Flight

  - Now consumes only 1 of each Martial Arts charge when cast
  - Now guarantee hits the target only if the Assassin has any Martial Arts charges
  - Casting Delay removed
  - Damage bonus per level increased from 25% to 35%

#### Shadow Disciplines

- Fade

  - Tooltip updated to now show the physical damage resist reduction

- Venom

  - Buff Duration scaling per level changed from 4 seconds to 12 seconds (matching Burst of Speed and Fade)

- Shadow Warrior

  - Casting Delay reduced from 6 seconds to 0.6 seconds
  - Casting Delay will no longer share its cooldown with other skills that have Casting Delays
  - Tooltip updated to now display Casting Delay

- Shadow Master

  - Casting Delay reduced from 6 seconds to 0.6 seconds
  - Casting Delay will no longer share its cooldown with other skills that have Casting Delays
  - Tooltip updated to now display Casting Delay

#### Traps

- Shock Web

  - Removed synergy from Death Sentry skill
  - Charged Bolt Sentry synergy increased from +11% to +17%
  - Lightning Sentry synergy increased from +11% to +17%
  - Casting Delay will no longer share its cooldown with other skills that have Casting Delays
  - Tooltip updated to now display Casting Delay

- Charged Bolt Sentry

  - Removed synergy from Death Sentry skill
  - Fire Blast synergy increased from +6% to +9%
  - Lightning Sentry synergy increased from +6% to +9%

- Lightning Sentry

  - Removed synergy from Death Sentry skill
  - Shock Web synergy increased from +12% to +18%
  - Charged Bolt Sentry synergy increased from +12% to +18%

- Fire Blast

  - Removed synergy from Death Sentry skill
  - Shock Web synergy increased from +9% to +11%
  - Charged Bolt Sentry synergy increased from +9% to +11%
  - Wake of Fire synergy increased from +9% to +11%
  - Lightning Sentry synergy increased from +9% to +11%
  - Wake of Inferno Sentry synergy increased from +9% to +11%

- Wake of Fire

  - Fire Blast synergy increased from +8% to +10%
  - Wake of Inferno synergy increased from +8% to +10%

- Wake of Inferno

  - Removed synergy from Death Sentry
  - Fire Blast synergy increased from +10% to +18%
  - Wake of Fire synergy increased from +7% to +18%

- Blade Sentinel

  - Casting Delay reduced from 2 seconds to 1 second
  - Casting Delay will no longer share its cooldown with other skills that have Casting Delays
  - Tooltip updated to now display Casting Delay
  - Missile speed increased by 20%
  - Weapon Damage increased from 37% to 75%
  - Blade Fury synergy added: +10% Damage per level
  - Blade Shield synergy added: +10% Damage per level

- Blade Fury

  - Blade Sentinel synergy added: +10% Damage per level
  - Blade Shield synergy added: +10% Damage per level

- Blade Shield

  - Duration baseline value increased from 20 seconds to 120 seconds (matching Burst of Speed and Fade)
  - Duration scaling per level increased from 5 seconds to 12 seconds (matching Burst of Speed and Fade)
  - Weapon Damage increased from 25% to 75%
  - Blade Sentinel synergy added: +10% Damage per level
  - Blade Fury synergy added: +10% Damage per level
  - Tooltip updated to now display Radius

## New Horadric Cube Recipes

- 1 Ral Rune + 1 Sol Rune + 1 Perfect Emerald + Normal Set Weapon = Exceptional Version of Set Weapon
- 1 Lum Rune + 1 Pul Rune + 1 Perfect Emerald + Exceptional Set Weapon = Elite Version of Set Weapon
- 1 Tal Rune + 1 Shael Rune + 1 Perfect Diamond + Normal Set Armor = Exceptional Version of Set Armor
- 1 Ko Rune + 1 Lem Rune + 1 Perfect Diamond + Exceptional Set Armor = Elite Version of Set Armor

## Set Item Bonus Changes

- Arcanna's Tricks

  - Increased +25 Mana to +50 Mana (2 Items)
  - Added Regenerate Mana 12% (3 Items)
  - Added +1 to All Skills (Full Set)

- Arctic Gear

  - Changed 6-14 Cold Damage to +2-198 to Maximum Cold Damage (+2 Per Character Level) (Full Set)

- Bul-Kathos' Children

  - Increased +20 Fire Damage to +200 Fire Damage (Full Set)
  - Increased +25 Defense to +200 Defense (Full Set)
  - Added 10% Life Stolen Per Hit (Full Set)
  - Added +20% Deadly Strike (Full Set)

- Cathan's Traps

  - Added Regenerate Mana 16% (2 Items)

- Civerb's Vestments

  - Increased Fire Resist +15% to Fire Resist +25% (2 Items)
  - Added 25% Bonus to Attack Rating (Full Set)
  - Added 4-396 Defense (+4 Per Character Level) (Full Set)

- Cow King's Leathers

  - Added 5-495 Defense (+5 Per Character Level) (2 Items)
  - Added +100 to Life (Full Set)
  - Added +1 To All Skills (Full Set)

- Infernal Tools

  - Added Maximum Mana 20% (Full Set)
  - Added Cannot Be Frozen (Full Set)

- Iratha's Finery

  - Added +24% Piercing Attack (3 Items)

- Milabrega's Regalia

  - Added +2-198 To Lightning Damage (+2 Per Character Level) (2 Items)
  - Added Cannot Be Frozen (3 Items)

- Naj's Ancient Vestige

  - Added 1-148% Better Chance of Getting Magic Items (+1.5 Per Character Level) (2 Items)
  - Increased Replenish Life +10 to Replenish Life +20 (Full Set)
  - Added +2 To Fire Skills (Full Set)
  - Added Increased Maximum Life 12% (Full Set)

- Sazabi's Grand Tribute

  - Added Poison Length Reduced by 75% (2 Items)
  - Added +1 To All Skills (Full Set)
  - Added Damage Reduced by 16% (Full Set)

- Vidala's Rig

  - Added 7% Mana Stolen Per Hit (2 Items)
  - Changed Adds 15-20 Cold Damage to +1-148 to Maximum Cold Damage (+1.5 Per Character Level) (Full Set)
