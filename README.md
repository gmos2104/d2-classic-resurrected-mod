# D2 Classic Resurrected Mod

Port of balance changes from D2 Resurrected to original D2

See [changelog](CHANGELOG.md) for implemented changes.

## Installation

Copy the `data` folder into your game root directory.

Add the following arguments to your game executable: `-direct -txt`
