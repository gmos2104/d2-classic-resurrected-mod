D2 to resurrected changes compared to original D2 (list taken from [blizard forums](https://us.forums.blizzard.com/en/d3/t/28-changes-in-d2-resurrected-more-post-launch/42973)). Some of these changes will be considered and some discarded.

1. Auto Gold Pick-up
2. Advanced Stats Screen (Shows total FCR, LL, Magic Find ext)
3. Stash size increased to 10x10 + Added 3 more Shared Tabs.
4. Gamble Refresh Button + Item no longer disappears when buying multiple of the same type.
5. Cow King Kill Causing Lock-out Removed
6. Ability to link items in the chat window to show other players.
7. ~No game creation limit~ Changes for this mod are offline mode focused
8. ~New Battlenet System~ Changes for this mod are offline mode focused
9. ~No character expiration~ Changes for this mod are offline mode focused
10. Ladder-Only Unique Items Unlocked for Non-ladder + Single Player (anni/torch) See #18
11. Ladder-Only Runewords + Cube Recipes Unlocked for Single Player + Non-ladder!! See #17
12. "Endgame" Events Unlocked for Single Player. (Uber Diablo spawned with 1 soj, Bnet is ~75-120)
13. ~Playable on Xbox, PS, Switch *with* cross-progression~ Changes for this mod are offline mode focused
14. ~Global servers~ Changes for this mod are offline mode focused
15. More Details for Mercenary abilities
16. Hold shift to compare items to equipped
17. Enable loot drop names to be up all the time instead of holding Alt.
18. Ctrl click items into stash instead of drag & drop
19. WSG, Ebug, Strength, NHAM(?) & Sin Trap Bugs Removed
20. Unique Appearance of all items now show properly on your character when equipped.
21. ~Character selection screen has ultra close-up of your character + A Detailed back-drop of current Act for increased immersion.~ This will not be implemented for obvious reasons :)
22. Wide-screen Support up to 19:9 (removal of black-bars while zoomed-in possible compromise) (control zoom while holding F + Mouse wheel.)
23. More Accessibility Options (controller support, change text size, colorblindness, etc)
24. ~TCP/IP removal~ Will not remove TCP/IP
25. Increased overall map visibility.
26. ~Completely New Graphics Engine with Remastered Cinematics and Sounds~ This will not be implemented for obvious reasons :)
27. Optional toggle for "Miss" text to appear on monsters for better combat feedback.
28. Added In-Game Clock.
